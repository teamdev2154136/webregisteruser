function validarVideo() {
    const archivoInput = document.getElementById('videoInput');
    const archivoRuta = archivoInput.value;
    const extPermitidas = /(.avi|.wmv|.mov|.flv|.rm|.mp4|.mkv|.mks|.3gpp)$/i;
    if (!extPermitidas.exec(archivoRuta)) {
        alert('Tiene que seleccionar un Video');
        archivoInput.value = '';
        return false;
    } else if (archivoInput.files) {
        if (archivoInput.files[0]) {
            const visor = new FileReader();
            visor.onload = function(e) {
                document.getElementById('visorArchivo').innerHTML =
                    '<embed src="' + String(e.target.result) + '" width="500" height="375" />';
            };
            visor.readAsDataURL(archivoInput.files[0]);
        }
    }

    return validarVideo()
}

function validarAudio() {
    const archivoInput = document.getElementById('audioInput');
    const archivoRuta = archivoInput.value;
    const extPermitidas = /(.mp3|.aiff|.mid|.midi|.wav|.wma|.OGG|.FLAC|.FLA)$/i;
    if (!extPermitidas.exec(archivoRuta)) {
        alert('Tiene que seleccionar un Audio');
        archivoInput.value = '';
        return false;
    } else if (archivoInput.files) {
        if (archivoInput.files[0]) {
            const visor = new FileReader();
            visor.onload = function(e) {
                document.getElementById('visorArchivo').innerHTML =
                    '<embed src="' + String(e.target.result) + '" width="500" height="375" />';
            };
            // Agrega una función toString() personalizada para el objeto FileReader.prototype
            FileReader.prototype.toString = function() {
                return 'FileReader';
            };
            visor.readAsDataURL(archivoInput.files[0]);
        }

    }

}

// Agrega una función toString() personalizada para el objeto HTMLElement.prototype
HTMLElement.prototype.toString = function() {
    return 'HTMLElement';
};

// Agrega una función toString() personalizada para el objeto FileReader.prototype
FileReader.prototype.toString = function() {
    return 'FileReader';
};

// Agrega una función toString() personalizada para el objeto HTMLElement.prototype
HTMLElement.prototype.toString = function() {
    return 'HTMLElement';
};